# FSE_projeto03

Victor Hugo Dias Coelho 16/0019401

## Configurar
Para configurar o ssid e a senha da rede, bem como as chaves de acesso das apis
é necessário rodar o comandado `idf.py menuconfig`.

Neste menu terá duas opções no qual tem que ser configurados:
1. Configuração do Wifi Configuration -> onde colocara a ssid e a senha,
atenção mantenha o número de tentativas em 1;

2. Configuração das chaves das API's -> onde colocara as chaves de acesso
das api's em questão. O nome da api está na opção no menu;

Após isso salve e continue para o build.

## Compilar e visualizar resultado   
Para isso use o comando `idf.py build && idf.py -p /dev/ttyUSB0 flash monitor`

## Resultado
A cada 5 minutos o console vai imprimir o seguinte:

`temp: <temperatura atual>	MIN: <temperatura minima>	MAX: <temperatura máxima>	HUM: <Humidade>`
