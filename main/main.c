#include <stdio.h>
#include "nvs_flash.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_http_client.h"
#include "esp_log.h"
#include "freertos/semphr.h"
#include "driver/gpio.h"

#include "wifi.h"
#include "http_client.h"

#define BLINK 2 

xSemaphoreHandle conexaoWifiSemaphore;
xSemaphoreHandle startBlink;
xSemaphoreHandle wifiTry;

void piscarLedConection(void * param)
{
        gpio_pad_select_gpio(BLINK);
        gpio_set_direction(BLINK, GPIO_MODE_OUTPUT);
        while(true)
        {

                gpio_set_level(BLINK, 1);
                vTaskDelay(500 / portTICK_PERIOD_MS);
                  xSemaphoreTake(startBlink, portMAX_DELAY);
                  gpio_set_level(BLINK, 0);
                  vTaskDelay(500 / portTICK_PERIOD_MS);
        }

}


void RealizaHTTPRequest(void * params)
{
  while(true)
  {
    ESP_LOGI("Main Task", "Esperando sinal wifi !");
    if(xSemaphoreTake(conexaoWifiSemaphore, (TickType_t) 10))
    {
      while(true)
      {
        ESP_LOGI("Main Task", "Realiza HTTP Request");
        get_weather();
        xSemaphoreGive(startBlink);
        vTaskDelay(300000 / portTICK_PERIOD_MS);
      }
    }
    else
    {
      xSemaphoreGive(startBlink);
    }
  }
}

void app_main(void)
{
    // Inicializa o NVS
    esp_err_t ret = nvs_flash_init();
    esp_log_level_set("*", ESP_LOG_ERROR);
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);
    
    
    conexaoWifiSemaphore = xSemaphoreCreateBinary();
    startBlink = xSemaphoreCreateBinary();
    wifiTry = xSemaphoreCreateBinary();
    wifi_start();
    xTaskCreate(&piscarLedConection,  "Pisca LED", 2048, NULL, 1, NULL);
    xTaskCreate(&RealizaHTTPRequest,  "Processa HTTP", 4096 * 2, NULL, 1, NULL);
}
