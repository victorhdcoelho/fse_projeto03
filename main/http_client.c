#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "http_client.h"
#include "cJSON.h"

#include "esp_event.h"
#include "esp_http_client.h"
#include "esp_log.h"

#define TAG "HTTP"
#define MAX_HTTP_RECV_BUFFER 512
#define MAX_HTTP_OUTPUT_BUFFER 2048

char latitude[400], longitude[400];
/* Root cert for howsmyssl.com, taken from howsmyssl_com_root_cert.pem
   The PEM file was extracted from the output of this command:
   openssl s_client -showcerts -connect www.howsmyssl.com:443 </dev/null
   The CA root cert is the last cert given in the chain of certs.
   To embed it in the app binary, the PEM file is named
   in the component.mk COMPONENT_EMBED_TXTFILES variable.
*/
esp_err_t _http_event_handle(esp_http_client_event_t *evt)
{
    static char *output_buffer;  // Buffer to store response of http request from event handler
    static int output_len;       // Stores number of bytes read
    switch(evt->event_id) {
        case HTTP_EVENT_ERROR:
            ESP_LOGD(TAG, "HTTP_EVENT_ERROR");
            break;
        case HTTP_EVENT_ON_CONNECTED:
            ESP_LOGD(TAG, "HTTP_EVENT_ON_CONNECTED");
            break;
        case HTTP_EVENT_HEADER_SENT:
            ESP_LOGD(TAG, "HTTP_EVENT_HEADER_SENT");
            break;
        case HTTP_EVENT_ON_HEADER:
            ESP_LOGD(TAG, "HTTP_EVENT_ON_HEADER, key=%s, value=%s", evt->header_key, evt->header_value);
            break;
        case HTTP_EVENT_ON_DATA:
            ESP_LOGD(TAG, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
            /*
             *  Check for chunked encoding is added as the URL for chunked encoding used in this example returns binary data.
             *  However, event handler can also be used in case chunked encoding is used.
             */
            // If user_data buffer is configured, copy the response into the buffer
            if (evt->user_data) {
                memcpy(evt->user_data + output_len, evt->data, evt->data_len);
            } else {
                if (output_buffer == NULL) {
                    output_buffer = (char *) malloc(esp_http_client_get_content_length(evt->client));
                    output_len = 0;
                    if (output_buffer == NULL) {
                        ESP_LOGE(TAG, "Failed to allocate memory for output buffer");
                        return ESP_FAIL;
                    }
                }
                memcpy(output_buffer + output_len, evt->data, evt->data_len);
            }
            output_len += evt->data_len;
            

            break;
        case HTTP_EVENT_ON_FINISH:
            ESP_LOGD(TAG, "HTTP_EVENT_ON_FINISH");
            if (output_buffer != NULL) {
                // Response is accumulated in output_buffer. Uncomment the below line to print the accumulated response
                // ESP_LOG_BUFFER_HEX(TAG, output_buffer, output_len);
                free(output_buffer);
                output_buffer = NULL;
            }
            output_len = 0;
            break;
        case HTTP_EVENT_DISCONNECTED:
            ESP_LOGI(TAG, "HTTP_EVENT_DISCONNECTED");
            // int mbedtls_err = 0;
            // esp_err_t err = esp_tls_get_and_clear_last_error(evt->data, &mbedtls_err, NULL);
            // if (err != 0) {
            //     if (output_buffer != NULL) {
            //         free(output_buffer);
            //         output_buffer = NULL;
            //     }
            //     output_len = 0;
            //     ESP_LOGI(TAG, "Last esp error code: 0x%x", err);
            //     ESP_LOGI(TAG, "Last mbedtls failure: 0x%x", mbedtls_err);
            // }
            break;
    }
    return ESP_OK;
}

void http_request_ipstack(){
    char local_response_buffer[MAX_HTTP_OUTPUT_BUFFER] = {0};
    char url[2000]={'\0'};
    strcat(url, "http://api.ipstack.com/179.214.49.228?access_key=");
    strcat(url, CONFIG_FIRST_KEY);
    
    esp_http_client_config_t config = {
        .url = url,
        .event_handler = _http_event_handle,
        .user_data = local_response_buffer,
    };
    esp_http_client_handle_t client = esp_http_client_init(&config);
    esp_err_t err = esp_http_client_perform(client);
    ESP_LOGI(TAG, "%s\n", local_response_buffer);
    if (err == ESP_OK) {
    cJSON * root = cJSON_Parse(local_response_buffer);
    if(!root)
    {
        ESP_LOGW(TAG, "Error before: [%s]\n",cJSON_GetErrorPtr());
    }
    cJSON * lat = cJSON_GetObjectItem(root, "latitude");
    if(!lat)
    {
        ESP_LOGW(TAG, "Error before: [%s]\n",cJSON_GetErrorPtr());
    }
    cJSON * lon = cJSON_GetObjectItem(root, "longitude");
    if(!lon)
    {
        ESP_LOGW(TAG, "Error before: [%s]\n",cJSON_GetErrorPtr());
    }
    // printf("%s\n", cJSON_Print(lat));
    strcpy(latitude, cJSON_Print(lat));
    strcpy(longitude, cJSON_Print(lon));
    
    ESP_LOGI(TAG, "latitude: %s", latitude);
    ESP_LOGI(TAG, "longitude: %s", longitude);
    ESP_LOGI(TAG, "Status = %d, content_length = %d",
            esp_http_client_get_status_code(client),
            esp_http_client_get_content_length(client));
    }
    esp_http_client_cleanup(client);
}

void http_request_weather(){
    char local_response_buffer[MAX_HTTP_OUTPUT_BUFFER] = {0};
    char url[2000]={'\0'};
    strcat(url, "http://api.openweathermap.org/data/2.5/weather?");
    strcat(url, "lat=");
    strcat(url, latitude);
    strcat(url, "&lon=");
    strcat(url, longitude);
    strcat(url, "&units=metric");
    strcat(url, "&appid=");
    strcat(url, CONFIG_SECOND_KEY);
    esp_http_client_config_t config = {
        .url = url,
        .event_handler = _http_event_handle,
        .user_data = local_response_buffer,
    };
    esp_http_client_handle_t client = esp_http_client_init(&config);
    esp_err_t err = esp_http_client_perform(client);

    ESP_LOGI(TAG, "%s\n", local_response_buffer);
    cJSON * root = cJSON_Parse(local_response_buffer);
    if (!root)
	{
		ESP_LOGW(TAG, "Error before: [%s]\n",cJSON_GetErrorPtr());
	}
    cJSON * temp_main = cJSON_GetObjectItem(root, "main");
    if (!temp_main)
	{
		ESP_LOGW(TAG, "Error before: [%s]\n",cJSON_GetErrorPtr());
	}
    cJSON * temp = cJSON_GetObjectItem(temp_main, "temp");
    cJSON * temp_min = cJSON_GetObjectItem(temp_main, "temp_min");
    cJSON * temp_max = cJSON_GetObjectItem(temp_main, "temp_max");
    cJSON * hum = cJSON_GetObjectItem(temp_main, "humidity");

    printf("TEMP: %s\tMIN: %s\tMAX: %s\tHUM: %s\n",cJSON_Print(temp), cJSON_Print(temp_min), cJSON_Print(temp_max), cJSON_Print(hum));
    if (err == ESP_OK) {
    ESP_LOGI(TAG, "Status = %d, content_length = %d",
            esp_http_client_get_status_code(client),
            esp_http_client_get_content_length(client));
    }
    esp_http_client_cleanup(client);
}

void get_weather()
{
    http_request_ipstack();
    http_request_weather();
}
